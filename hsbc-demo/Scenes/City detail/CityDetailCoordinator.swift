//
//  CityDetailCoordinator.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

final class CityDetailCoordinator: Coordinator {

    private let cityModel: CityModel
    var didClose: ((_ cityID: ID) -> Void)?

    init(appContext: AppContextProtocol, cityModel: CityModel, presentationStyle: CoordinatorPresentationStyle) {
        self.cityModel = cityModel
        let viewModel = CityDetailViewModel(cityModel: cityModel, citiesFavoritesService: appContext.citiesFavoritesService)
        let viewController = CityDetailViewController(viewModel: viewModel)
        super.init(viewController: viewController, appContext: appContext, presentationStyle: presentationStyle)
        viewModel.coordinatorDelegate = self
    }
}

extension CityDetailCoordinator: CityDetailViewModelCoordinatorDelegate {
    func close() {
        dismiss()
        didClose?(cityModel.id)
    }

    func navigateToVisitorsList(_ visitors: [CityVisitor]) {
        let coordinator = CityVisitorsCoordinator(appContext: appContext, visitors: visitors, presentationStyle: .push)
        start(coordinator: coordinator)
    }
}
