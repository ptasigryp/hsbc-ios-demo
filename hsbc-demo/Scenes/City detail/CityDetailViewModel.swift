//
//  CityDetailViewModel.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

protocol CityDetailViewModelCoordinatorDelegate: class {
    func close()
    func navigateToVisitorsList(_ visitors: [CityVisitor])
}

protocol CityDetailViewModelDelegate: ViewModelDelegateProtocol {
    func cityRatingLoaded()
    func cityVisitorsLoaded()
    func cityFavoriteStateChanged(isFavorite: Bool)
}

protocol CityDetailViewModelProtocol: class {
    var delegate: CityDetailViewModelDelegate? { get set }
    var cityImageURL: URL { get }
    var cityName: String { get }
    var isFavorite: Bool { get }
    var cityRatingText: String { get }
    var cityVisitorsText: String { get }
    var navigationBarModel: NavigationBarModel { get }

    func didLoad()
    func favoriteButtonTapped()
    func visitorsLabelTapped()
}

final class CityDetailViewModel: CityDetailViewModelProtocol {

    weak var coordinatorDelegate: CityDetailViewModelCoordinatorDelegate?
    weak var delegate: CityDetailViewModelDelegate?

    private let networkService: CityDetailsNetworkServiceProtocol
    private let citiesFavoritesService: CitiesFavoritesServiceProtocol
    private let cityModel: CityModel
    private var cityRating: CityRating?
    private var cityVisitors: [CityVisitor]?
    private lazy var closeBarButtonItem: UIBarButtonItem = {
        UIBarButtonItem(image: Asset.closeButton.image, style: .plain, target: self, action: #selector(closeButtonTapped))
    }()

    var cityImageURL: URL { cityModel.thumbnailURL }
    var cityName: String { cityModel.name }
    var isFavorite: Bool { citiesFavoritesService.isCityFavorite(cityID: cityModel.id) }
    var cityRatingText: String {
        guard let rating = cityRating else { return L10n.cityDetailsRatingUnknown }
        return L10n.cityDetailsRating(rating.averageRatingText, "\(rating.numberOfVotes)")
    }
    var cityVisitorsText: String {
        let visitorsCountText: String
        if let count = cityVisitors?.count {
            visitorsCountText = "\(count)"
        } else {
            visitorsCountText = "---"
        }
        return L10n.cityDetailsVisitors(visitorsCountText)
    }
    var navigationBarModel: NavigationBarModel {
        NavigationBarModel(title: .empty, leftButton: closeBarButtonItem, rightButtons: [])
    }

    init(cityModel: CityModel,
         networkService: CityDetailsNetworkServiceProtocol = CityDetailsNetworkService(),
         citiesFavoritesService: CitiesFavoritesServiceProtocol) {
        self.cityModel = cityModel
        self.networkService = networkService
        self.citiesFavoritesService = citiesFavoritesService
    }

    private func loadVisitors(completion: @escaping  (Bool) -> Void) {
        networkService.fetchCityVisitors(cityID: cityModel.id) { [weak self] result in
            switch result {
            case .success(let visitors):
                self?.cityVisitors = visitors
                self?.delegate?.cityVisitorsLoaded()
                completion(true)
            case .failure:
                completion(false)
            }
        }
    }

    private func loadRating(completion: @escaping  (Bool) -> Void) {
        networkService.fetchCityRating(cityID: cityModel.id) { [weak self] result in
            switch result {
            case .success(let rating):
                self?.cityRating = rating
                self?.delegate?.cityRatingLoaded()
                completion(true)
            case .failure:
                completion(false)
            }
        }
    }

    private func loadData() {
        delegate?.setLoadingState(loading: true)

        var anyErrors: Bool = false
        let group = DispatchGroup()

        group.enter()
        loadVisitors { success in
            if success == false { anyErrors = true }
            group.leave()
        }

        group.enter()
        loadRating { success in
            if success == false { anyErrors = true }
            group.leave()
        }

        group.notify(queue: .main) { [weak self] in
            guard let self = self else { return }
            self.delegate?.setLoadingState(loading: false)
            if anyErrors {
                let alertModel = AlertModel.tryAgainAlertModel { [weak self] in
                    self?.loadData()
                }
                self.delegate?.showAlert(model: alertModel)
            }
        }
    }

    @objc private func closeButtonTapped() {
        coordinatorDelegate?.close()
    }

    func didLoad() {
        loadData()
    }

    func favoriteButtonTapped() {
        let newValue = self.citiesFavoritesService.toggleCityFavorite(cityID: cityModel.id)
        delegate?.cityFavoriteStateChanged(isFavorite: newValue)
    }

    func visitorsLabelTapped() {
        guard let visitors = cityVisitors, visitors.isEmpty == false else { return }
        coordinatorDelegate?.navigateToVisitorsList(visitors)
    }
}
