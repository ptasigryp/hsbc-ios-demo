//
//  CityDetailViewController.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

final class CityDetailViewController: UIViewController, CityDetailViewModelDelegate {

    @IBOutlet private var cityImageView: UIImageView!
    @IBOutlet private var cityNameLabel: UILabel!
    @IBOutlet private var ratingLabel: UILabel!
    @IBOutlet private var visitorsLabel: UILabel!
    @IBOutlet private var favoriteButton: FavoriteButton!

    private let viewModel: CityDetailViewModelProtocol

    init(viewModel: CityDetailViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        viewModel.delegate = self
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.configure(with: viewModel.navigationBarModel)
        cityImageView.layer.masksToBounds = true
        cityImageView.setImage(from: viewModel.cityImageURL, animated: true)
        cityNameLabel.text = viewModel.cityName
        favoriteButton.isMarked = viewModel.isFavorite
        setupRatingLabel()
        setupVisitorsLabel()

        visitorsLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(visitorsLabelTapped)))

        viewModel.didLoad()
    }

    private func setupRatingLabel() {
        ratingLabel.text = viewModel.cityRatingText
    }

    private func setupVisitorsLabel() {
        visitorsLabel.text = viewModel.cityVisitorsText
    }

    @IBAction private func favoriteButtonTapped(_ sender: Any) {
        viewModel.favoriteButtonTapped()
    }

    @objc private func visitorsLabelTapped() {
        viewModel.visitorsLabelTapped()
    }

    func cityRatingLoaded() {
        setupRatingLabel()
    }

    func cityVisitorsLoaded() {
        setupVisitorsLabel()
    }

    func cityFavoriteStateChanged(isFavorite: Bool) {
        favoriteButton.isMarked = isFavorite
    }
}
