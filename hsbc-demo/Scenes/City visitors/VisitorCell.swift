//
//  VisitorCell.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

final class VisitorCell: UITableViewCell, ReusableView {

    struct Configurator {
        let nameText: String
    }

    private struct Constants {
        static let padding: UIEdgeInsets = .init(top: 0, left: 16, bottom: 0, right: 16)
    }

    private lazy var separatorView: UIView = {
        let color: UIColor
        if #available(iOS 13.0, *) {
            color = .separator
        } else {
            color = .lightGray
        }
        return UIView(backgroundColor: color)
    }()
    private lazy var nameLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 17, weight: .regular)
        view.numberOfLines = 1
        return view
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        contentView.addSubview(nameLabel)
        contentView.addSubview(separatorView)
        nameLabel.fillSuperview(padding: Constants.padding)
        separatorView.anchor(.leading(contentView.leadingAnchor, constant: Constants.padding.left),
                             .trailing(contentView.trailingAnchor, constant: Constants.padding.right),
                             .bottom(contentView.bottomAnchor, constant: 0),
                             .height(1))
    }

    func configure(with configurator: Configurator) {
        nameLabel.text = configurator.nameText
    }
}
