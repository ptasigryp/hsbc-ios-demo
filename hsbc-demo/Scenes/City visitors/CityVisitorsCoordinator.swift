//
//  CityVisitorsCoordinator.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

final class CityVisitorsCoordinator: Coordinator {

    init(appContext: AppContextProtocol, visitors: [CityVisitor], presentationStyle: CoordinatorPresentationStyle) {
        let viewModel = CityVisitorsViewModel(visitors: visitors)
        let viewController = CityVisitorsViewController(viewModel: viewModel)
        super.init(viewController: viewController, appContext: appContext, presentationStyle: presentationStyle)
    }
}
