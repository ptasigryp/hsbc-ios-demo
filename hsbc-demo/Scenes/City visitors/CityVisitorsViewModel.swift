//
//  CityVisitorsViewModel.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

protocol CityVisitorsViewModelProtocol {
    var numberOfCells: Int { get }

    func cellConfigurator(at indexPath: IndexPath) -> VisitorCell.Configurator
}

final class CityVisitorsViewModel: CityVisitorsViewModelProtocol {

    private let visitors: [CityVisitor]
    var numberOfCells: Int { visitors.count }

    init(visitors: [CityVisitor]) {
        self.visitors = visitors
    }

    func cellConfigurator(at indexPath: IndexPath) -> VisitorCell.Configurator {
        VisitorCell.Configurator(nameText: visitors[indexPath.row].fullName)
    }
}
