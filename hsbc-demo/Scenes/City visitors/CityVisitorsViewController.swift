//
//  CityVisitorsViewController.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

final class CityVisitorsViewController: UIViewController, UITableViewDataSource {

    @IBOutlet private var tableView: UITableView!

    private let viewModel: CityVisitorsViewModelProtocol

    init(viewModel: CityVisitorsViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(VisitorCell.self)
        tableView.rowHeight = 50
        tableView.dataSource = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfCells
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: VisitorCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configure(with: viewModel.cellConfigurator(at: indexPath))
        return cell
    }
}
