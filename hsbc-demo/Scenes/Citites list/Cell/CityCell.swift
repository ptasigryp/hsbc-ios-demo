//
//  CityCell.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

final class CityCell: UITableViewCell, ReusableView, NibLoadable {

    private struct Constants {
        static let cornerRadius: CGFloat = 8.0
    }

    @IBOutlet private var containerView: UIView!
    @IBOutlet private var cityImageView: UIImageView!
    @IBOutlet private var titleBackgroundView: UIView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var favoriteButton: FavoriteButton!

    private var viewModel: CityCellViewModel?

    override func awakeFromNib() {
        super.awakeFromNib()
        accessibilityIdentifier = "CityCell"
        cityImageView.clipsToBounds = true
        cityImageView.layer.masksToBounds = true
        containerView.layer.cornerRadius = Constants.cornerRadius
        containerView.layer.masksToBounds = true
        favoriteButton.setImage(Asset.star.image, for: .normal)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        cityImageView.cancelImageRequest()
        cityImageView.image = nil
    }

    private func updateFavoriteState(isFavorite: Bool) {
        favoriteButton.isMarked = isFavorite
    }

    @IBAction private func favoriteButtonTapped(_ sender: Any) {
        viewModel?.favoriteButtonTapped()
    }

    func configure(with viewModel: CityCellViewModel) {
        self.viewModel = viewModel

        titleLabel.text = viewModel.name
        updateFavoriteState(isFavorite: viewModel.isFavorite)
        cityImageView.setImage(from: viewModel.thumbnailURL)

        viewModel.onChange = { [weak self] state in
            switch state {
            case .isFavorite(let isFavorite):
                self?.updateFavoriteState(isFavorite: isFavorite)
            }
        }
    }
}
