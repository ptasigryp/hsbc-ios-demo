//
//  CityCellViewModel.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

final class CityCellViewModel {

    enum State {
        case isFavorite(Bool)
    }

    var onFavoriteTapAction: EmptyAction?
    var onChange: Action<State>?
    let name: String
    let thumbnailURL: URL
    var isFavorite: Bool {
        didSet {
            onChange?(.isFavorite(isFavorite))
        }
    }

    init(name: String, thumbnailURL: URL, isFavorite: Bool, onFavoriteTapAction: EmptyAction? = nil) {
        self.name = name
        self.thumbnailURL = thumbnailURL
        self.isFavorite = isFavorite
        self.onFavoriteTapAction = onFavoriteTapAction
    }

    func favoriteButtonTapped() {
        onFavoriteTapAction?()
    }
}

extension CityCellViewModel {
    convenience init(from cityModel: CityModel, isFavorite: Bool, onFavoriteTapAction: EmptyAction? = nil) {
        self.init(name: cityModel.name, thumbnailURL: cityModel.thumbnailURL,
                  isFavorite: isFavorite, onFavoriteTapAction: onFavoriteTapAction)
    }
}
