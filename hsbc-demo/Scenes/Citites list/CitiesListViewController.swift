//
//  CitiesListViewController.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

final class CitiesListViewController: UIViewController, CitiesListViewModelDelegate {

    private struct Constants {
        static let optionsTransitionTime: TimeInterval = 0.5
        static let optionsMaskedCorners: CACornerMask = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        static let optionsCornerRadius: CGFloat = 5.0
        static let optionsOffset: CGFloat = 100.0
    }

    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var optionsHoverView: UIView!
    @IBOutlet private var optionsView: UIView!
    @IBOutlet private var favoritesOptionLabel: UILabel!
    @IBOutlet private var favoritesSwitch: UISwitch!

    private let viewModel: CitiesListViewModelProtocol
    private var optionsListVisible: Bool = false

    init(viewModel: CitiesListViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        viewModel.delegate = self
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupAccessbility()
        navigationItem.configure(with: viewModel.navigationBarModel)

        tableView.registerCell(CityCell.self)
        tableView.rowHeight = 200.0
        tableView.dataSource = self
        tableView.delegate = self
        setOptionsListVisible(false, animated: false)
        optionsHoverView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(optionsHoverViewTapped)))
        favoritesSwitch.isOn = viewModel.onlyFavoriteCitiesFilterEnabled
        favoritesOptionLabel.text = L10n.citiesListShowOnlyFavoritesFilter
        optionsView.layer.maskedCorners = Constants.optionsMaskedCorners
        optionsView.layer.cornerRadius = Constants.optionsCornerRadius

        viewModel.didLoad()
    }

    @IBAction private func favoriteSwitchAction(_ sender: Any) {
        setOptionsListVisible(false, animated: true)
        viewModel.setOnlyFavoritesFilterEnabled(favoritesSwitch.isOn)
    }

    @objc private func optionsHoverViewTapped() {
        setOptionsListVisible(false, animated: true)
    }

    private func setupAccessbility() {
        view.accessibilityIdentifier = "CitiesList"
        tableView.accessibilityIdentifier = "CitiesListTableView"
        favoritesSwitch.accessibilityIdentifier = "FavoritesSwitch"
        optionsView.accessibilityIdentifier = "OptionsView"
    }

    private func setOptionsListVisible(_ visible: Bool, animated: Bool) {
        let block = {
            self.optionsHoverView.alpha = visible ? 1.0 : 0.0
            self.optionsView.alpha = visible ? 1.0 : 0.0
            self.optionsView.transform = visible ? .identity : CGAffineTransform(translationX: 0, y: Constants.optionsOffset)
        }
        animated ? UIView.animate(withDuration: Constants.optionsTransitionTime, animations: block) : block()
        optionsListVisible = visible
    }

    func reloadList() {
        tableView.reloadSections(IndexSet(arrayLiteral: 0), with: .automatic)
    }

    func reloadRows(at: [IndexPath]) {
        tableView.reloadRows(at: at, with: .none)
    }

    func toggleOptionsVisibily() {
        setOptionsListVisible(!optionsListVisible, animated: true)
    }
}

extension CitiesListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfCells
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CityCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configure(with: viewModel.cellModel(at: indexPath))
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectCell(at: indexPath)
    }
}
