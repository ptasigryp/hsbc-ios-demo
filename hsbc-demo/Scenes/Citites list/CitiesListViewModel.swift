//
//  CitiesListViewModel.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

protocol CitiesListViewModelDelegate: ViewModelDelegateProtocol {
    func reloadList()
    func reloadRows(at: [IndexPath])
    func toggleOptionsVisibily()
}

protocol CitiesListViewModelCoordinatorDelegate: class {
    func navigateToCityDetail(cityModel: CityModel)
}

protocol CitiesListViewModelProtocol: class {
    var delegate: CitiesListViewModelDelegate? { get set }
    var numberOfCells: Int { get }
    var navigationBarModel: NavigationBarModel { get }
    var onlyFavoriteCitiesFilterEnabled: Bool { get }
    func didLoad()
    func setOnlyFavoritesFilterEnabled(_ enabled: Bool)
    func cellModel(at indexPath: IndexPath) -> CityCellViewModel
    func didSelectCell(at indexPath: IndexPath)
}

final class CitiesListViewModel: CitiesListViewModelProtocol {

    private let networkService: CitiesListNetworkServiceProtocol
    private let citiesFavoritesService: CitiesFavoritesServiceProtocol
    private var citiesModels: [CityModel] = [] {
        didSet {
            updateFilteredCitiesModels()
        }
    }
    private var filteredCitiesModels: [CityModel] = []
    private(set) var onlyFavoriteCitiesFilterEnabled: Bool = false

    weak var delegate: CitiesListViewModelDelegate?
    weak var coordinatorDelegate: CitiesListViewModelCoordinatorDelegate?

    var numberOfCells: Int { filteredCitiesModels.count }
    var navigationBarModel: NavigationBarModel {
        NavigationBarModel(title: .title(L10n.citiesListTitle), leftButton: nil, rightButtons: [filtersBarButtonItem])
    }
    private lazy var filtersBarButtonItem: UIBarButtonItem = {
        UIBarButtonItem(image: Asset.filter.image, style: .plain, target: self, action: #selector(filtersButtonTapped))
    }()

    init(networkService: CitiesListNetworkServiceProtocol = CitiesListNetworkService(),
         citiesFavoritesService: CitiesFavoritesServiceProtocol) {
        self.networkService = networkService
        self.citiesFavoritesService = citiesFavoritesService
        updateFilteredCitiesModels()
    }

    private func updateFilteredCitiesModels() {
        filteredCitiesModels = onlyFavoriteCitiesFilterEnabled ?
            citiesModels.filter { citiesFavoritesService.isCityFavorite(cityID: $0.id) } :
            citiesModels
    }

    private func loadData() {
        delegate?.setLoadingState(loading: true)
        networkService.fetchCititesList { [weak self] result in
            guard let self = self else { return }
            self.delegate?.setLoadingState(loading: false)
            switch result {
            case .success(let citiesModels):
                self.citiesModels = citiesModels
                self.delegate?.reloadList()
            case .failure:
                let alertModel = AlertModel.tryAgainAlertModel { [weak self] in
                    self?.loadData()
                }
                self.delegate?.showAlert(model: alertModel)
            }
        }
    }

    @objc private func filtersButtonTapped() {
        delegate?.toggleOptionsVisibily()
    }

    func reloadCityCell(for cityID: ID) {
        guard let index = citiesModels.firstIndex(where: { $0.id == cityID }) else { return }
        delegate?.reloadRows(at: [IndexPath(row: index, section: 0)])
    }

    func didLoad() {
        loadData()
    }

    func setOnlyFavoritesFilterEnabled(_ enabled: Bool) {
        onlyFavoriteCitiesFilterEnabled = enabled
        updateFilteredCitiesModels()
        delegate?.reloadList()
    }

    func cellModel(at indexPath: IndexPath) -> CityCellViewModel {
        let cityModel = filteredCitiesModels[indexPath.row]
        let cellViewModel = CityCellViewModel(from: cityModel,
                                              isFavorite: citiesFavoritesService.isCityFavorite(cityID: cityModel.id))
        cellViewModel.onFavoriteTapAction = { [weak self, unowned cellViewModel] in
            guard let self = self else { return }
            let newValue = self.citiesFavoritesService.toggleCityFavorite(cityID: cityModel.id)
            cellViewModel.isFavorite = newValue
        }
        return cellViewModel
    }

    func didSelectCell(at indexPath: IndexPath) {
        coordinatorDelegate?.navigateToCityDetail(cityModel: citiesModels[indexPath.row])
    }
}
