//
//  CitiesListCoordinator.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

final class CitiesListCoordinator: Coordinator {

    private let citiesListViewModel: CitiesListViewModel

    init(appContext: AppContextProtocol) {
        let viewModel = CitiesListViewModel(citiesFavoritesService: appContext.citiesFavoritesService)
        let viewController = CitiesListViewController(viewModel: viewModel)
        self.citiesListViewModel = viewModel
        super.init(viewController: viewController, appContext: appContext, presentationStyle: .root)
        viewModel.coordinatorDelegate = self
    }
}

extension CitiesListCoordinator: CitiesListViewModelCoordinatorDelegate {
    func navigateToCityDetail(cityModel: CityModel) {
        let coordinator = CityDetailCoordinator(appContext: appContext, cityModel: cityModel, presentationStyle: .modal)
        start(coordinator: coordinator)

        // needs to refresh cell, viewWillAppear won't be called because of modal presentation style
        coordinator.didClose = { [weak self] cityID in
            self?.citiesListViewModel.reloadCityCell(for: cityID)
        }
    }
}
