//
//  CitiesFavoritesService.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

protocol CitiesFavoritesServiceProtocol {
    func isCityFavorite(cityID: ID) -> Bool
    func setCityFavorite(cityID: ID, isFavorite: Bool)
    func toggleCityFavorite(cityID: ID) -> Bool
}

final class CitiesFavoritesService: CitiesFavoritesServiceProtocol {

    private let userDefaults: DefaultsStoring
    private var favoriteCities: Set<ID>

    init(userDefaults: DefaultsStoring = UserDefaults.standard) {
        self.userDefaults = userDefaults
        favoriteCities = Set((try? userDefaults.get(objectType: [ID].self, forKey: DefaultsKey.favoriteCities.description)) ?? [])
    }

    @discardableResult
    private func saveFavorites() -> Bool {
        do {
            try userDefaults.set(object: Array(favoriteCities), forKey: DefaultsKey.favoriteCities.description)
        } catch {
            return false
        }
        return true
    }

    func isCityFavorite(cityID: ID) -> Bool {
        favoriteCities.contains(cityID)
    }

    func setCityFavorite(cityID: ID, isFavorite: Bool) {
        let hasChanges: Bool
        if isFavorite {
            (hasChanges, _) = favoriteCities.insert(cityID)
        } else {
            let oldElement = favoriteCities.remove(cityID)
            hasChanges = oldElement != nil
        }

        guard hasChanges else { return }
        saveFavorites()
    }

    @discardableResult
    func toggleCityFavorite(cityID: ID) -> Bool {
        let isFavorite = isCityFavorite(cityID: cityID)
        let newValue = !isFavorite
        setCityFavorite(cityID: cityID, isFavorite: newValue)
        return newValue
    }
}
