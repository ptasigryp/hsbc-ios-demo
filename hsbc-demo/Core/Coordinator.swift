//
//  Coordinators.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

enum CoordinatorPresentationStyle {
    case push
    case modal
    case root
}

protocol CoordinatorProtocol: class, UINavigationControllerDelegate, UIAdaptivePresentationControllerDelegate {
    var supercoordinator: CoordinatorProtocol? { get set }
    var subcoordinators: [CoordinatorProtocol] { get set }
    var presentationStyle: CoordinatorPresentationStyle { get }
    var viewController: UIViewController { get }
    var navigationController: UINavigationController? { get }

    func pop(coordinator: CoordinatorProtocol)
    func removeFromSupercoordinator()
    func findCoordinator(forViewController viewController: UIViewController) -> CoordinatorProtocol?
    func subcoordinator<T>(ofType coordinatorType: T.Type) -> T? where T: CoordinatorProtocol

    func insert(coordinator: CoordinatorProtocol?)
    func removeSubcoordinators()
    func start(coordinator: CoordinatorProtocol, animated: Bool)
    func dismiss(animated: Bool, completion: (() -> Void)?)
}

class Coordinator: NSObject, CoordinatorProtocol {

    weak var supercoordinator: CoordinatorProtocol?
    var subcoordinators: [CoordinatorProtocol] = []
    let presentationStyle: CoordinatorPresentationStyle
    let appContext: AppContextProtocol
    let viewController: UIViewController

    var navigationController: UINavigationController? {
        viewController as? UINavigationController ??
            viewController.navigationController ??
            supercoordinator?.viewController.navigationController
    }

    init(viewController: UIViewController,
         appContext: AppContextProtocol,
         presentationStyle: CoordinatorPresentationStyle) {
        self.viewController = viewController
        self.appContext = appContext
        self.presentationStyle = presentationStyle
        super.init()
    }

    func pop(coordinator: CoordinatorProtocol) {
        guard let index = subcoordinators.firstIndex(where: { $0 === coordinator }) else { return }
        subcoordinators.remove(at: index)
        coordinator.navigationController?.delegate = self
    }

    func removeFromSupercoordinator() {
        supercoordinator?.pop(coordinator: self)
    }

    func findCoordinator(forViewController viewController: UIViewController) -> CoordinatorProtocol? {
        weak var supercoordinator: CoordinatorProtocol? = self.supercoordinator
        while let coordinator = supercoordinator {
            if viewController === coordinator.viewController {
                return coordinator
            }
            supercoordinator = coordinator.supercoordinator
        }
        return nil
    }

    func subcoordinator<T>(ofType coordinatorType: T.Type) -> T? where T: CoordinatorProtocol {
        subcoordinators.first { $0 is T } as? T
    }

    func insert(coordinator: CoordinatorProtocol? = nil) {
        if let coordinator = coordinator {
            coordinator.supercoordinator = self
            subcoordinators.append(coordinator)
        }
        coordinator?.navigationController?.delegate = coordinator
    }

    func removeSubcoordinators() {
        subcoordinators.forEach {
            $0.removeSubcoordinators()
        }
        subcoordinators.removeAll()
    }

    func start(coordinator: CoordinatorProtocol, animated: Bool = true) {
        switch coordinator.presentationStyle {
        case .modal:
            let navigationController = UINavigationController(rootViewController: coordinator.viewController)
            navigationController.presentationController?.delegate = coordinator
            viewController.present(navigationController, animated: animated, completion: nil)
        case .push:
            navigationController?.pushViewController(coordinator.viewController, animated: animated)
        case .root:
            navigationController?.delegate = coordinator
            navigationController?.setViewControllers([coordinator.viewController], animated: animated)
            removeSubcoordinators()
        }
        insert(coordinator: coordinator)
    }

    func dismiss(animated: Bool = true, completion: (() -> Void)? = nil) {

        let finalize = { [weak self] in
            self?.subcoordinators.removeAll()
            completion?()
            self?.removeFromSupercoordinator()
        }

        switch presentationStyle {
        case .push:
            navigationController?.popViewController(animated: true)
            finalize()
        case .modal:
            navigationController?.dismiss(animated: animated, completion: {
                finalize()
            })
        case .root:
            finalize()
        }
    }

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        controller.presentedViewController.modalPresentationStyle
    }

    func navigationController(_ navigationController: UINavigationController,
                              didShow viewController: UIViewController, animated: Bool) {
        if let coordinator = findCoordinator(forViewController: viewController) {
            coordinator.subcoordinators.removeAll()
            navigationController.delegate = coordinator
        }
    }

    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        subcoordinators.removeAll()
        removeFromSupercoordinator()
    }
}
