//
//  AppCoordinator.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

final class AppCoordinator {

    private struct Constants {
        static let startTransitionTime: TimeInterval = 0.3
    }

    private let window: UIWindow
    private let appContext: AppContextProtocol
    private var childCoordinator: CoordinatorProtocol?

    init(appContext: AppContextProtocol, window: UIWindow) {
        self.appContext = appContext
        self.window = window
    }

    func start() {
        //Implement any logic you want for start of your application, eg. if user is not logged in - present login flow, otherwise open main screen
        let citiesListCoordinator = CitiesListCoordinator(appContext: appContext)
        start(coordinator: citiesListCoordinator, with: window, animated: false)
    }

    private func start(coordinator: CoordinatorProtocol, with window: UIWindow, animated: Bool) {
        window.rootViewController = UINavigationController(rootViewController: coordinator.viewController)
        window.makeKeyAndVisible()
        childCoordinator = coordinator
        if animated {
            UIView.transition(with: window, duration: Constants.startTransitionTime,
                              options: .transitionCrossDissolve, animations: nil, completion: nil)
        }
    }
}
