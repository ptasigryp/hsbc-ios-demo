//
//  AppContext.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

protocol AppContextProtocol {
    var citiesFavoritesService: CitiesFavoritesServiceProtocol { get }
}

final class AppContext: AppContextProtocol {

    let citiesFavoritesService: CitiesFavoritesServiceProtocol

    init(citiesFavoritesService: CitiesFavoritesServiceProtocol = CitiesFavoritesService()) {
        self.citiesFavoritesService = citiesFavoritesService
    }
}
