// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable function_parameter_count identifier_name line_length type_body_length
internal enum L10n {
  /// Cancel
  internal static let cancelButton = L10n.tr("Localizable", "CancelButton")
  /// Close
  internal static let closeButton = L10n.tr("Localizable", "CloseButton")
  /// OK
  internal static let okButton = L10n.tr("Localizable", "OkButton")
  /// Try again
  internal static let tryAgainButton = L10n.tr("Localizable", "TryAgainButton")
  /// Filter
  internal static let citiesListFilter = L10n.tr("Localizable", "CitiesList.filter")
  /// Display only favorites
  internal static let citiesListShowOnlyFavoritesFilter = L10n.tr("Localizable", "CitiesList.showOnlyFavoritesFilter")
  /// Cities list
  internal static let citiesListTitle = L10n.tr("Localizable", "CitiesList.title")
  /// Rating: %@ (%@ total votes)
  internal static func cityDetailsRating(_ p1: String, _ p2: String) -> String {
    return L10n.tr("Localizable", "CityDetails.rating", p1, p2)
  }
  /// Unknown rating
  internal static let cityDetailsRatingUnknown = L10n.tr("Localizable", "CityDetails.ratingUnknown")
  /// Number of visitors: %@
  internal static func cityDetailsVisitors(_ p1: String) -> String {
    return L10n.tr("Localizable", "CityDetails.visitors", p1)
  }
  /// Something went wrong
  internal static let errorGenericMessage = L10n.tr("Localizable", "Error.generic.message")
  /// Error
  internal static let errorGenericTitle = L10n.tr("Localizable", "Error.generic.title")
}
// swiftlint:enable function_parameter_count identifier_name line_length type_body_length

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
