//
//  NibLoadable.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

protocol NibLoadable {
    static var nibName: String { get }
}

extension NibLoadable {
    static var nibName: String {
        String(describing: self)
    }
}

extension NibLoadable where Self: UIView {
    static func loadFromNib() -> Self {
        let nib = UINib(nibName: Self.nibName, bundle: nil)
        guard  let view = nib.instantiate(withOwner: nil, options: nil).first as? Self else {
            fatalError("First view in nib is not a \(Self.nibName) class.")
        }

        return view
    }

    private var viewFromNib: UIView {
        let nib = UINib(nibName: Self.nibName, bundle: nil)
        guard let view = nib.instantiate(withOwner: self, options: nil).first as? UIView else {
            fatalError("Cannot instantiate \(Self.nibName) from nib.")
        }
        return view
    }

    func loadNib() {
        let view = viewFromNib
        addSubview(view)
        view.fillSuperview()
    }
}
