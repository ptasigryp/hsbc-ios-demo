//
//  Typealiases.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

typealias ID = String

typealias Handler<T> = (Result<T, Error>) -> Void
typealias EmptyResponse = Result<Void, Error>
typealias EmptyHandler = (EmptyResponse) -> Void

typealias Action<T> = ((T) -> Void)
typealias EmptyAction = (() -> Void)
