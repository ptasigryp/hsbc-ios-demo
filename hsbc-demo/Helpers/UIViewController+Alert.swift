//
//  UIViewController+Alert.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

extension UIViewController {
    func displayAlert(using model: AlertModel, withHapticFeedback: Bool = false) {
        let alert = UIAlertController(title: model.title,
                                      message: model.message,
                                      preferredStyle: .alert)
        model.buttons.forEach { model in
            let action = UIAlertAction(
                title: model.title,
                style: model.style) { action in
                    model.action?()
            }
            alert.addAction(action)
        }
        if withHapticFeedback {
            let generator = UINotificationFeedbackGenerator()
            generator.prepare()
            generator.notificationOccurred(.warning)
        }
        present(alert, animated: true)
    }
}
