//
//  UIView+Shadow.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

struct Shadow {
    let color: UIColor
    let radius: CGFloat
    let offset: CGSize
    let opacity: Float

    static let light: Shadow = Shadow(color: .gray,
                                      radius: 4,
                                      offset: CGSize(width: 0, height: 2),
                                      opacity: 0.07)
    static let dark: Shadow = Shadow(color: .black,
                                     radius: 4,
                                     offset: CGSize(width: 0, height: 2),
                                     opacity: 0.34)
    static let floating: Shadow = Shadow(color: .black,
                                         radius: 6,
                                         offset: CGSize(width: 0, height: 6),
                                         opacity: 0.24)
    static let small: Shadow = Shadow(color: .black,
                                      radius: 2,
                                      offset: CGSize(width: 0, height: 1),
                                      opacity: 0.2)
    static let tiny: Shadow = Shadow(color: .black,
                                     radius: 1,
                                     offset: CGSize(width: 0, height: 2),
                                     opacity: 0.1)
    static let floatingLarge: Shadow = Shadow(color: UIColor(red: 0.56, green: 0.56, blue: 0.56, alpha: 1),
                                              radius: 24,
                                              offset: CGSize(width: 0, height: 3),
                                              opacity: 0.18)
    static let dropDownPicker: Shadow = Shadow(color: .black,
                                               radius: 4,
                                               offset: CGSize(width: 0, height: 4),
                                               opacity: 0.24)
}

extension UIView {
    func applyShadow(_ shadow: Shadow) {
        self.setupShadow(opacity: shadow.opacity, radius: shadow.radius, offset: shadow.offset, color: shadow.color)
    }
}
