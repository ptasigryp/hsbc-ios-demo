//
//  FavoriteButton.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

class FavoriteButton: UIButton {

    var isMarked: Bool = false {
        didSet {
            tintColor = isMarked ? .orange : .lightGray
        }
    }

    init(isMarked: Bool) {
        super.init(frame: .zero)
        commonInit()
        self.isMarked = isMarked
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        setImage(Asset.star.image, for: .normal)
        isMarked = false
    }
}
