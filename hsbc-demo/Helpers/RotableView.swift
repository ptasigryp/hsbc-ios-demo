//
//  RotableView.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

enum RotationDirection: Double {
    case clockwise = 1.0
    case counterClockwise = -1.0
}

protocol RotableView {
    var isRotating: Bool { get }
    func startRotating(animationTime: Float,
                       direction: RotationDirection,
                       velocity rotatesPerSecond: Double,
                       timingFunction: CAMediaTimingFunction)
    func stopRotating()
}

extension RotableView where Self: UIView {

    private var animationKey: String { "rotationAnimation" }

    var isRotating: Bool {
        layer.animation(forKey: animationKey) != nil
    }

    func startRotating(animationTime: Float = .infinity,
                       direction: RotationDirection,
                       velocity rotatesPerSecond: Double,
                       timingFunction: CAMediaTimingFunction = CAMediaTimingFunction(name: .linear)) {
        guard isRotating == false else { return }
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotationAnimation.fromValue = 0.0
        rotationAnimation.toValue = direction.rawValue * rotatesPerSecond * .pi * 2
        rotationAnimation.duration = 1.0
        rotationAnimation.repeatCount = animationTime
        rotationAnimation.timingFunction = timingFunction
        layer.add(rotationAnimation, forKey: animationKey)
    }

    func stopRotating() {
        layer.removeAnimation(forKey: animationKey)
    }
}

final class RotableImageView: UIImageView, RotableView {}
