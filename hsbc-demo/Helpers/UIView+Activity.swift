//
//  UIView+Activity.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

final class ActivityIndicatorContainer: UIView {

    var isEnabled: Bool = false {
        didSet {
            isEnabled ? start() : stop()
        }
    }

    var isAnimating: Bool {
        activityIndicatorView.isRotating
    }

    private lazy var activityIndicatorView: LoadingIndicator = {
        let imageView = LoadingIndicator()
        imageView.addConstraint(imageView.widthAnchor.constraint(lessThanOrEqualToConstant: 48))
        imageView.addConstraint(imageView.heightAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: 1))
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }

    private func initialize() {
        backgroundColor = .clear

        addSubview(activityIndicatorView)
        activityIndicatorView.centerInSuperview()
    }

    private func start() {
        isHidden = false
        alpha = 0.0
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 1.0
        })
        activityIndicatorView.startRotating(direction: .clockwise, velocity: 1.0)
    }

    private func stop() {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0.0
        }, completion: { finished in
            guard finished else { return }
            self.isHidden = true
            self.activityIndicatorView.stopRotating()
        })
    }
}

final class LoadingIndicator: UIImageView, RotableView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    convenience init() {
        self.init(frame: .zero)
    }

    private func commonInit() {
        self.image = Asset.loadingSpinner.image
        self.tintColor = .blue
    }
}

protocol LoadingDisplayable {
    var isDisplayingLoading: Bool { get }
    func setDisplayingLoading(state: Bool)
}

extension UIView: LoadingDisplayable {

    private var loadingSpinnerContainer: ActivityIndicatorContainer? {
        subviews.compactMap { $0 as? ActivityIndicatorContainer }.first
    }

    var isDisplayingLoading: Bool {
        loadingSpinnerContainer?.isAnimating ?? false
    }

    func setDisplayingLoading(state: Bool) {
        if let container = loadingSpinnerContainer {
            container.isEnabled = state
        } else {
            let container = ActivityIndicatorContainer()
            addSubview(container)
            container.fillSuperview()
            container.isEnabled = state
        }
    }
}
