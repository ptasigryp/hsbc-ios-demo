//
//  DefaultsStoring.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

protocol DefaultsStoring: AnyObject {
    func set<T: Codable>(object: T, forKey: String) throws
    func get<T: Codable>(objectType: T.Type, forKey: String) throws -> T?
    subscript(key: DefaultsKey) -> Any? { get set }
    subscript(bool key: DefaultsKey) -> Bool { get set }
    subscript(string key: DefaultsKey) -> String? { get set }
    subscript(int key: DefaultsKey) -> Int { get set }
}

extension UserDefaults: DefaultsStoring {
    subscript(key: DefaultsKey) -> Any? {
        get { value(forKey: key.description) }
        set { set(newValue, forKey: key.description) }
    }

    subscript(int key: DefaultsKey) -> Int {
        get { integer(forKey: key.description) }
        set { set(newValue, forKey: key.description) }
    }

    subscript(bool key: DefaultsKey) -> Bool {
        get { bool(forKey: key.description) }
        set { set(newValue, forKey: key.description) }
    }

    subscript(string key: DefaultsKey) -> String? {
        get { string(forKey: key.description) }
        set { set(newValue, forKey: key.description) }
    }

    func set<T: Codable>(object: T, forKey: String) throws {
        let jsonData = try JSONEncoder().encode(object)
        set(jsonData, forKey: forKey)
    }

    func get<T: Codable>(objectType: T.Type, forKey: String) throws -> T? {
        guard let result = value(forKey: forKey) as? Data else {
            return nil
        }
        return try JSONDecoder().decode(objectType, from: result)
    }
}

enum DefaultsKey: String {

    case favoriteCities

    var description: String { rawValue }
}
