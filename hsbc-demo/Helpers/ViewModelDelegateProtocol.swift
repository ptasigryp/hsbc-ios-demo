//
//  ViewModelDelegateProtocol.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

protocol ViewModelDelegateProtocol: AlertDisplayingProtocol, LoadingStateProtocol {}
