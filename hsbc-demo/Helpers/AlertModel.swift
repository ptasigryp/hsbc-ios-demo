//
//  AlertModel.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

struct AlertButtonModel {
    typealias AlertButtonAction = (() -> Void)
    let title: String
    let action: AlertButtonAction?
    let style: UIAlertAction.Style
    init(title: String,
         style: UIAlertAction.Style = .default,
         action: AlertButtonAction? = nil) {
        self.title = title
        self.action = action
        self.style = style
    }

    static let okButton: AlertButtonModel = AlertButtonModel(title: L10n.okButton)
}

struct AlertModel {
    let title: String
    let message: String
    let buttons: [AlertButtonModel]
}

extension AlertModel {
    static func infoAlertModel(title: String, message: String, cancelAction: (() -> Void)? = nil) -> AlertModel {
        let okButton = AlertButtonModel(title: L10n.okButton, action: cancelAction)
        return AlertModel(title: title, message: message, buttons: [okButton])
    }

    static func tryAgainAlertModel(title: String = L10n.errorGenericTitle,
                                   message: String = L10n.errorGenericMessage,
                                   cancelAction: (() -> Void)? = nil,
                                   tryAgainAction: @escaping () -> Void) -> AlertModel {
        let buttons = [
            AlertButtonModel(title: L10n.cancelButton, style: .cancel) {
                cancelAction?()
            },
            AlertButtonModel(title: L10n.tryAgainButton) {
                tryAgainAction()
            }
        ]
        return AlertModel(title: title,
                          message: message,
                          buttons: buttons)
    }
}
