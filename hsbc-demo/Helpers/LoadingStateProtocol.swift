//
//  LoadingStateProtocol.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

protocol LoadingStateProtocol: class {
    func setLoadingState(loading: Bool)
}

extension LoadingStateProtocol where Self: UIViewController {
    func setLoadingState(loading: Bool) {
        view.setDisplayingLoading(state: loading)
    }
}
