//
//  NavigationBarModel.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

enum NavigationBarTitle {
    case title(String)
    case titleView(UIView)
    case empty
}

struct NavigationBarModel {
    let title: NavigationBarTitle
    let leftButton: UIBarButtonItem?
    let rightButtons: [UIBarButtonItem]?

    init(title: NavigationBarTitle, leftButton: UIBarButtonItem? = nil, rightButtons: [UIBarButtonItem] = []) {
        self.title = title
        self.leftButton = leftButton
        self.rightButtons = rightButtons
    }
}

extension UINavigationItem {

    func configure(with model: NavigationBarModel, animated: Bool = true) {
        switch model.title {
        case .title(let title):
            self.title = title
        case .titleView(let titleView):
            self.titleView = titleView
        case .empty:
            break
        }

        setLeftBarButton(model.leftButton, animated: animated)
        setRightBarButtonItems(model.rightButtons, animated: animated)
    }
}
