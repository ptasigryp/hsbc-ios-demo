//
//  AlertDisplaying.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

protocol AlertDisplayingProtocol: class {
    func showAlert(model: AlertModel)
}

extension AlertDisplayingProtocol where Self: UIViewController {
    func showAlert(model: AlertModel) {
        self.displayAlert(using: model)
    }
}
