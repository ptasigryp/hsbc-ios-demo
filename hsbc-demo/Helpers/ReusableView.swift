//
//  ReusableView.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation
import class UIKit.UIView

protocol ReusableView {}

extension ReusableView where Self: UIView {
    static var identifier: String {
        String(describing: self)
    }
}
