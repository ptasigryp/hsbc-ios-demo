//
//  ApiServerConfiguration.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

final class ApiServerConfiguration: ServerConfigurationProtocol {
    let baseURL: URL = URL(string: "https://jsonkeeper.com")! //swiftlint:disable:this force_unwrapping
}
