//
//  NetworkServiceProtocol.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

protocol NetworkServiceProtocol {
    var session: URLSession { get }
    var baseURL: URL { get }
}

extension NetworkServiceProtocol {
    func buildURL(path: String, queryItems: [URLQueryItem]) -> URL? {
        guard var urlComponents = URLComponents(url: baseURL, resolvingAgainstBaseURL: true) else { return nil }
        urlComponents.path = path
        urlComponents.queryItems = queryItems
        return urlComponents.url
    }

    func buildRequest<T>(path: String,
                         queryItems: [URLQueryItem] = [],
                         method: HTTPMethod = .get,
                         responseType: T.Type) throws -> Request<T> {
        guard let url = buildURL(path: path, queryItems: queryItems) else {
            throw NetworkError.invalidUrl
        }
        return Request<T>(url: url, method: method, session: session)
    }
}
