//
//  CitiesListNetworkService.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

protocol CitiesListNetworkServiceProtocol {
    func fetchCititesList(completion: @escaping Handler<[CityModel]>)
}

final class CitiesListNetworkService: CitiesListNetworkServiceProtocol, NetworkServiceProtocol {

    var session: URLSession
    var baseURL: URL

    init(session: URLSession = .shared, serverConfiguration: ServerConfigurationProtocol = ApiServerConfiguration()) {
        self.session = session
        self.baseURL = serverConfiguration.baseURL
    }

    func fetchCititesList(completion: @escaping (Result<[CityModel], Error>) -> Void) {
        do {
            let request = try buildRequest(path: "/b/N6ZS", responseType: CitiesListResponse.self)
            request.perform { result in
                switch result {
                case .success(let response):
                    completion(.success(response.cities))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        } catch {
            completion(.failure(error))
        }
    }
}
