//
//  CitiesListNetworkServiceMock.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

/*
{
 "cities": [
    {
        "id": "abcdef1",
        "name": "Warsaw",
        "thumbnailURL": "https://bit.ly/3kDCTK8"
    },
    {
        "id": "abcdef2",
        "name": "Berlin",
        "thumbnailURL": "https://bit.ly/2Q0s8DP"
    },
    {
        "id": "abcdef3",
        "name": "Paris",
        "thumbnailURL": "https://bit.ly/33YfXPR"
    },
    {
        "id": "abcdef4",
        "name": "London",
        "thumbnailURL": "https://bit.ly/31KqziF"
    },
    {
        "id": "abcdef5",
        "name": "Zurich",
        "thumbnailURL": "https://bit.ly/3ivMoZS"
    },
    {
        "id": "abcdef6",
        "name": "Budapest",
        "thumbnailURL": "https://bit.ly/31M0jEr"
    },
    {
        "id": "abcdef7",
        "name": "Oslo",
        "thumbnailURL": "https://bit.ly/3gSGh1z"
    },
    {
        "id": "abcdef8",
        "name": "Stockholm",
        "thumbnailURL": "https://bit.ly/33T1tAT"
    },
    {
        "id": "abcdef9",
        "name": "Roma",
        "thumbnailURL": "https://bit.ly/3kAXajH"
    },
    {
        "id": "abcdef11",
        "name": "Kiev",
        "thumbnailURL": "https://bit.ly/30OJppE"
    }
 ]
}
*/

//swiftlint:disable force_unwrapping
final class CitiesListNetworkServiceMock: CitiesListNetworkServiceProtocol {
    func fetchCititesList(completion: @escaping (Result<[CityModel], Error>) -> Void) {
        let models: [CityModel] = [
            .init(id: "abcdef1", name: "New York", thumbnailURL: URL(string: "https://bit.ly/3gOGov6")!),
            .init(id: "abcdef2", name: "Los Angeles", thumbnailURL: URL(string: "https://bit.ly/3gOGov6")!),
            .init(id: "abcdef3", name: "Szczecin", thumbnailURL: URL(string: "https://bit.ly/3gOGov6")!),
            .init(id: "abcdef4", name: "Seul", thumbnailURL: URL(string: "https://bit.ly/3gOGov6")!),
            .init(id: "abcdef5", name: "Budapest", thumbnailURL: URL(string: "https://bit.ly/3gOGov6")!)
        ]
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            completion(.success(models))
        }
    }
}
//swiftlint:enable force_unwrapping
