//
//  CitiesListRequest.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

struct CityModel: Decodable {
    let id: ID
    let name: String
    let thumbnailURL: URL
}

struct CitiesListResponse: Decodable {
    let cities: [CityModel]
}
