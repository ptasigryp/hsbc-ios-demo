//
//  CityDetailsNetworkServiceMock.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

/*
{
 "visitors": [
    {
        "firstName": "Michael",
        "lastName": "Bear"
    },
    {
        "firstName": "Jacob",
        "middleName": "Adam",
        "lastName": "Goat"
    },
    {
        "firstName": "Michael",
        "lastName": "De Santa"
    },
    {
        "firstName": "Katalina",
        "lastName": "Vialpando"
    },
    {
        "firstName": "Carl",
        "lastName": "Johnsons"
    }
 ]
}
 
{
 "rating": {
    "averageRating": 4.56999999,
    "numberOfVotes": 1235678
 }
}
*/

final class CityDetailsNetworkServiceMock: CityDetailsNetworkServiceProtocol {
    func fetchCityRating(cityID: ID, completion: @escaping Handler<CityRating>) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            completion(.success(.init(averageRating: 4.5, numberOfVotes: 1234)))
        }
    }

    func fetchCityVisitors(cityID: ID, completion: @escaping Handler<[CityVisitor]>) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            completion(.success([.init(firstName: "Michael", middleName: nil, lastName: "Bear"),
                                 .init(firstName: "Jacob", middleName: "Adam", lastName: "Goat"),
                                 .init(firstName: "Michael", middleName: nil, lastName: "De Santa"),
                                 .init(firstName: "Katalina", middleName: nil, lastName: "Vialpando"),
                                 .init(firstName: "Carl", middleName: nil, lastName: "Johnson")]))
        }
    }
}
