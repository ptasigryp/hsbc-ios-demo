//
//  CityDetailsNetworkService.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

protocol CityDetailsNetworkServiceProtocol {
    func fetchCityVisitors(cityID: ID, completion: @escaping Handler<[CityVisitor]>)
    func fetchCityRating(cityID: ID, completion: @escaping Handler<CityRating>)
}

final class CityDetailsNetworkService: CityDetailsNetworkServiceProtocol, NetworkServiceProtocol {

    var session: URLSession
    var baseURL: URL

    init(session: URLSession = .shared, serverConfiguration: ServerConfigurationProtocol = ApiServerConfiguration()) {
        self.session = session
        self.baseURL = serverConfiguration.baseURL
    }

    func fetchCityVisitors(cityID: ID, completion: @escaping Handler<[CityVisitor]>) {
        do {
            let request = try buildRequest(path: "/b/PBGY", responseType: CityVisitorsResponse.self)
            request.perform { result in
                switch result {
                case .success(let response):
                    completion(.success(response.visitors))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        } catch {
            completion(.failure(error))
        }
    }

    func fetchCityRating(cityID: ID, completion: @escaping Handler<CityRating>) {
        do {
            let request = try buildRequest(path: "/b/EK32", responseType: CityRatingResponse.self)
            request.perform { result in
                switch result {
                case .success(let response):
                    completion(.success(response.rating))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        } catch {
            completion(.failure(error))
        }
    }
}
