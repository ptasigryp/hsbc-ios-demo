//
//  CityVisitorsResponse.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

struct CityVisitor: Decodable {
    let firstName: String
    let middleName: String?
    let lastName: String

    var fullName: String {
        [firstName, middleName, lastName].compactMap { $0 }.joined(separator: " ")
    }
}

struct CityVisitorsResponse: Decodable {
    let visitors: [CityVisitor]
}
