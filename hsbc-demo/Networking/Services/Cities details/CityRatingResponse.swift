//
//  CityRatingResponse.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

struct CityRating: Decodable {
    let averageRating: Float
    let numberOfVotes: Int

    var averageRatingText: String {
        String(format: "%.2f", averageRating)
    }
}

struct CityRatingResponse: Decodable {
    let rating: CityRating
}
