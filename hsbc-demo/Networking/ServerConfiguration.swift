//
//  ServerConfiguration.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

protocol ServerConfigurationProtocol {
    var baseURL: URL { get }
    //In future this can contain additional stuff like default cookies
}
