//
//  NetworkError.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case invalidUrl
    case invalidResponse
    case badRequest
    case serverError
    case unknown
}
