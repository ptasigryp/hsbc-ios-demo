//
//  Request.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import Foundation

protocol RequestProtocol {
    associatedtype ResponseType
    typealias ResultCompletion = Handler<ResponseType>

    var url: URL { get }
    var method: HTTPMethod { get }
    var session: URLSession { get }
}

struct Request<ResponseType>: RequestProtocol {
    let url: URL
    let method: HTTPMethod
    let session: URLSession

    init(url: URL, method: HTTPMethod, session: URLSession) {
        self.url = url
        self.method = method
        self.session = session
    }
}

extension RequestProtocol {
    private func handleResponse(data: Data?, statusCode: Int?, error: Error?) -> Result<Data, Error> {
        switch (data, statusCode, error) {
        case (let data, .some(let statusCode), _) where (200...299).contains(statusCode):
            if let data = data, !data.isEmpty {
                return .success(data)
            } else {
                return .failure(NetworkError.invalidResponse)
            }
        case (_, .some(let statusCode), _) where (400...499).contains(statusCode):
            return .failure(NetworkError.badRequest)
        case (_, .some(let statusCode), _) where (500...599).contains(statusCode):
            return .failure(NetworkError.serverError)
        default: //swiftlint:disable:this switch_default_case
            return .failure(NetworkError.unknown)
        }
    }
}

extension RequestProtocol where Self.ResponseType: Decodable {
    func perform(completion: @escaping ResultCompletion) {
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        let task = session.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                let httpResponse = response as? HTTPURLResponse
                let statusCode = httpResponse?.statusCode
                let result = self.handleResponse(data: data, statusCode: statusCode, error: error)
                switch result {
                case .success(let data):
                    do {
                        let model = try JSONDecoder().decode(ResponseType.self, from: data)
                        completion(.success(model))
                    } catch {
                        completion(.failure(error))
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }

        task.resume()
    }
}
