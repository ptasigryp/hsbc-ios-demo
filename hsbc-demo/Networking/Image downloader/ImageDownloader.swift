//
//  ImageDownloader.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

protocol ImageDownloaderProtocol {
    func download(from url: URL, completion: @escaping Handler<UIImage>) -> URLSessionDataTask
    func setImage(for imageView: UIImageView, url: URL, animated: Bool)
    func cancelRequest(for imageView: UIImageView)
}

final class ImageDownloader: ImageDownloaderProtocol {

    static let shared: ImageDownloader = ImageDownloader()

    private let cache: ImageCacheProtocol
    private let session: URLSession
    private var queue: [UIImageView: URLSessionDataTask] = [:]

    init(cache: ImageCacheProtocol = ImageCache.shared, session: URLSession = .shared) {
        self.cache = cache
        self.session = session
    }

    private func dataTask(from url: URL, completion: @escaping (Result<UIImage, Error>) -> Void) -> URLSessionDataTask {
        let task = session.dataTask(with: url) { [weak self] data, _, _ in
            DispatchQueue.main.async {
                guard let data = data, let image = UIImage(data: data) else {
                    completion(.failure(NetworkError.invalidResponse))
                    return
                }
                self?.cache.add(image: image, for: url)
                completion(.success(image))
            }
        }
        return task
    }

    private func setImage(_ image: UIImage, for imageView: UIImageView, animated: Bool) {
        let block = {
            imageView.image = image
        }
        animated ? UIView.transition(with: imageView,
                                     duration: 0.3,
                                     options: .transitionCrossDissolve,
                                     animations: block, completion: nil) : block()
    }

    func download(from url: URL, completion: @escaping (Result<UIImage, Error>) -> Void) -> URLSessionDataTask {
        let task = dataTask(from: url, completion: completion)
        task.resume()
        return task
    }

    func setImage(for imageView: UIImageView, url: URL, animated: Bool) {
        if let image = cache.image(for: url) {
            setImage(image, for: imageView, animated: false)
            return
        }
        if let oldTask = queue[imageView] {
            oldTask.cancel()
        }
        let task = download(from: url) { [weak self] result in
            switch result {
            case .success(let image):
                self?.setImage(image, for: imageView, animated: animated)
            case .failure:
                break
            }
            self?.queue.removeValue(forKey: imageView)
        }
        queue[imageView] = task
    }

    func cancelRequest(for imageView: UIImageView) {
        queue[imageView]?.cancel()
    }
}
