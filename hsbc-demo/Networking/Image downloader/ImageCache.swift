//
//  ImageCache.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

protocol ImageCacheProtocol {
    func add(image: UIImage, for url: URL)
    func remove(for url: URL) -> Bool
    func image(for url: URL) -> UIImage?
}

final class ImageCache: ImageCacheProtocol {

    /*
     Possible improvements:
     -Store image last use timestamp
     -Implement coroutine that clears unusued images from memory
     -Add optional disk storage for after-restart caching
     */

    static let shared: ImageCache = ImageCache()

    private var urlImageDictionary: [URL: UIImage] = [:]

    func add(image: UIImage, for url: URL) {
        urlImageDictionary[url] = image
    }

    func remove(for url: URL) -> Bool {
        urlImageDictionary.removeValue(forKey: url) != nil
    }

    func image(for url: URL) -> UIImage? {
        urlImageDictionary[url]
    }
}
