//
//  UIImageView+ImageDownloader.swift
//  hsbc-demo
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import UIKit

extension UIImageView {
    func setImage(from url: URL, downloader: ImageDownloaderProtocol = ImageDownloader.shared, animated: Bool = true) {
        downloader.setImage(for: self, url: url, animated: animated)
    }

    func cancelImageRequest(downloader: ImageDownloaderProtocol = ImageDownloader.shared) {
        downloader.cancelRequest(for: self)
    }
}
