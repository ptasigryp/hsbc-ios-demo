//
//  hsbc_demoUITests.swift
//  hsbc-demoUITests
//
//  Created by Michał Mańkus on 12/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import XCTest

class OpenDetailsUITests: XCTestCase {
    var app: XCUIApplication!

    override func setUp() {
        super.setUp()

        continueAfterFailure = false

        app = XCUIApplication()

        app.launchArguments.append("--uitesting")
    }

    func testOpeningCity() {
        app.launch()

        XCTAssertTrue(app.otherElements["CitiesList"].exists)

        let tableView = app.tables.matching(identifier: "CitiesListTableView")
        let cell = tableView.cells.element(matching: .cell, identifier: "CityCell").firstMatch
        cell.tap()

        let cityDetailView = app.otherElements["CityDetail"]
        XCTAssert(cityDetailView.waitForExistence(timeout: 5))
    }
}
