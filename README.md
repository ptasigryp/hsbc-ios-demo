# HSBC Demo iOS

_Cities list_ for iOS (Apple iPhone)

## About the app

This is demo app which displays list of few European Capitals, with ability to view some detail info and list of visitors

## Architecture

The project uses MVVM architecture with addition of Coordinators

### General overview

Every ViewController has ViewModel, which handles handles all logic of the view. Very simple views without logic have nested `Configurator` which is configuration what should be displayed on view.
Scene is created in coordinator, every scene should have dedicated coordinator class. 

### AppContext

`AppContext` is a class which combines all services that are used in application. It's created at the start of application in `AppDelegate`, and is passed to each coordinator creating view models with services that they require. Currently the only service used across the applications is `CitiesFavoritesService`

### Coordinators

Coordinators manage flow of the applications, they can start other coordinators with different `presentationStyle`. They support iOS 13 modal presentation style.
`ApplicationCoordinator` handles main view displayed in UIWindow. This helps with management of application state eg. user logs out and UIWindow should display login flow.

## Setup

* Install Xcode
* Install Cocoapods
* Run `$ pod install`from the root directory.
* Open hsbc-demo.xcworkspace in Xcode

### Requirements:
- Xcode 11.0
- CocoaPods 1.9.1
- Fastlane

### Fastlane
Fastlane is used for automated testing
Run `$ fastlane tests` from the root directory to run UI and Unit tests.

### Swiftlint
Install `swiftlint` to follow code style and guidelines.
```sh
$ brew install swiftlint
```

### Cocoapods
Run `$ pod install`from the root directory.

# Authors

* [Michał Mańkus](mailto:michmankus@gmail.com)