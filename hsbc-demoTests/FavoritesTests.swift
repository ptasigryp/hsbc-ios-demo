//
//  favoritesTests.swift
//  hsbc-demoTests
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import XCTest
@testable import HSBC_demo

final class DefaultsStoringMock: DefaultsStoring {
    func set<T>(object: T, forKey: String) throws where T: Decodable, T: Encodable {}
    func get<T>(objectType: T.Type, forKey: String) throws -> T? where T: Decodable, T: Encodable { nil }

    subscript(key: DefaultsKey) -> Any? {
        get { nil }
        set {}
    }

    subscript(bool key: DefaultsKey) -> Bool {
        get { false }
        set {}
    }

    subscript(string key: DefaultsKey) -> String? {
        get { nil }
        set {}
    }

    subscript(int key: DefaultsKey) -> Int {
        get { 0 }
        set {}
    }
}

class FavoritesTests: XCTestCase {

    private let defaultsStoring: DefaultsStoring = DefaultsStoringMock()

    func testStoringFavorite() {
        let favoritesService = CitiesFavoritesService(userDefaults: defaultsStoring)

        let idA = "random_id_A"
        let idB = "random_id_B"
        favoritesService.setCityFavorite(cityID: idA, isFavorite: true)
        favoritesService.setCityFavorite(cityID: idB, isFavorite: true)

        XCTAssertTrue(favoritesService.isCityFavorite(cityID: idA))
        XCTAssertTrue(favoritesService.isCityFavorite(cityID: idB))
    }

    func testTogglingFavorite() {
        let favoritesService = CitiesFavoritesService(userDefaults: defaultsStoring)

        let id = "random_id_A"
        favoritesService.setCityFavorite(cityID: id, isFavorite: true)
        XCTAssertTrue(favoritesService.isCityFavorite(cityID: id))

        favoritesService.toggleCityFavorite(cityID: id)
        XCTAssertFalse(favoritesService.isCityFavorite(cityID: id))
    }

    func testRemovingFavorite() {
        let favoritesService = CitiesFavoritesService(userDefaults: defaultsStoring)

        let id = "random_id_A"
        favoritesService.setCityFavorite(cityID: id, isFavorite: true)
        XCTAssertTrue(favoritesService.isCityFavorite(cityID: id))

        favoritesService.setCityFavorite(cityID: id, isFavorite: false)
        XCTAssertFalse(favoritesService.isCityFavorite(cityID: id))
    }
}
