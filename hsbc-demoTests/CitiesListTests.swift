//
//  citiesListTests.swift
//  hsbc-demoTests
//
//  Created by Michał Mańkus on 13/08/2020.
//  Copyright © 2020 mmankus. All rights reserved.
//

import XCTest
@testable import HSBC_demo

//swiftlint:disable force_unwrapping
final class CitiesListNetworkServiceTestMock: CitiesListNetworkServiceProtocol {
    func fetchCititesList(completion: @escaping (Result<[CityModel], Error>) -> Void) {
        let models: [CityModel] = [
            .init(id: "abcdef1", name: "New York", thumbnailURL: URL(string: "https://bit.ly/3gOGov6")!),
            .init(id: "abcdef2", name: "Los Angeles", thumbnailURL: URL(string: "https://bit.ly/3gOGov6")!),
            .init(id: "abcdef3", name: "Szczecin", thumbnailURL: URL(string: "https://bit.ly/3gOGov6")!),
            .init(id: "abcdef4", name: "Seul", thumbnailURL: URL(string: "https://bit.ly/3gOGov6")!),
            .init(id: "abcdef5", name: "Budapest", thumbnailURL: URL(string: "https://bit.ly/3gOGov6")!)
        ]
        completion(.success(models))
    }
}
//swiftlint:enable force_unwrapping

final class CitiesFavoritesTestMock: CitiesFavoritesServiceProtocol {

    private var favorites = Set<ID>(["abcdef1", "abcdef4"])

    func isCityFavorite(cityID: ID) -> Bool {
        favorites.contains(cityID)
    }

    func setCityFavorite(cityID: ID, isFavorite: Bool) {
        if isFavorite {
            _ = favorites.insert(cityID)
        } else {
            _ = favorites.remove(cityID)
        }
    }

    func toggleCityFavorite(cityID: ID) -> Bool {
        let isFavorite = isCityFavorite(cityID: cityID)
        let newValue = !isFavorite
        setCityFavorite(cityID: cityID, isFavorite: newValue)
        return newValue
    }
}

class CitiesListTests: XCTestCase {

    let viewModel = CitiesListViewModel(networkService: CitiesListNetworkServiceTestMock(),
                                        citiesFavoritesService: CitiesFavoritesTestMock())

    override func setUp() {
        viewModel.didLoad()
        super.setUp()
    }

    func testModelsCount() {
        XCTAssertEqual(viewModel.numberOfCells, 5)
    }

    func testModelFavoriteAtIndex() {
        let model = viewModel.cellModel(at: IndexPath(item: 3, section: 0))
        XCTAssertTrue(model.isFavorite)
    }

    func testModelNameAtIndex() {
        let model = viewModel.cellModel(at: IndexPath(item: 2, section: 0))
        XCTAssertEqual(model.name, "Szczecin")
    }

    func testOnlyFavorites() {
        viewModel.setOnlyFavoritesFilterEnabled(true)
        XCTAssertEqual(viewModel.numberOfCells, 2)
        viewModel.setOnlyFavoritesFilterEnabled(false)
        XCTAssertEqual(viewModel.numberOfCells, 5)
    }
}
